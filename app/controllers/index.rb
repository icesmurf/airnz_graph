Automation::Ocd.controllers :index do

  get :index, :map => '/' do
  #   session[:foo] = 'bar'
     render 'index'
  end


  get :arrival_stats, :map => '/arrival_stats' do
    get_arrival_stats
  end

  get :departure_stats, :map => '/departure_stats' do
    get_departure_stats
  end

  # get :sample, :map => '/sample/url', :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   "Maps to url '/foo/#{params[:id]}'"
  # end

  # get '/example' do
  #   'Hello world!'
  # end
  

end
