# Helper methods defined here can be accessed in any controller or view in the application

module Automation
  class Ocd
    module StatsHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers StatsHelper
  end
end
