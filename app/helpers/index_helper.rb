# Helper methods defined here can be accessed in any controller or view in the application

module Automation
  class Ocd
    module IndexHelper
      # def simple_helper_method
      # ...
      # end

      def _parse_page(agent)
      table =  agent.css("table[aria-label='Results table']")
      array = []

      table.css('tr').each do |row|
        airport = row.css('span.airport').text
        flight_numbers = row.css('span.flight-number').text
        flight_status = row.css('span.flight-status').text
        next if airport.empty?
        
        array << { airport: airport, flight_numbers: flight_numbers, status: flight_status }
      end

      return array
    end

    def get_arrival_stats

      arrivals_url  = "http://www.airnewzealand.co.nz/arrivals-and-departures?type=A&airport=AKLD"
      
      return _jsonify_stats(arrivals_url)
    end

    def get_departure_stats

      departing_url = "http://www.airnewzealand.co.nz/arrivals-and-departures?type=D&airport=AKLD"
      
      return _jsonify_stats(departing_url)
    end

    def _jsonify_stats( url )

      results = _parse_page( Mechanize.new.get(url) )
      
      stats = {}

      results.each do |stat|
        key = stat[:status]
        stats[key] = 0 if not stats.has_key?( key )
        stats[key] += 1
      end

      results = [] 
      results << [ 'Status', 'Count']
      stats.keys.each do |key|
        results << [ key, stats[key] ]
      end

      return results.to_json

    end

    end

    helpers IndexHelper
  end
end
