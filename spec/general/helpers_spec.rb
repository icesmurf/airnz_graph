require_relative '../spec_helper'

include Automation::Ocd::IndexHelper

describe "get_arrival_stats" do 
  it "should retreive the stats" do 
    res = JSON.parse(get_arrival_stats)
    expect(res.class).to eq(Array)
  end 
end
