require_relative '../spec_helper'

describe "get '/'" do
  it "should have bootstrap" do
    get '/'
    expect(last_response.body).to include("bootstrap.css")
    expect(last_response.body).to include("bootstrap.js")
  end
end

